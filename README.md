#### Facebook Birthday Greeter

## Requirement
- Selenium
- Chromedriver

## How to run
- Open Terminal
- Go inside the project folder
- Set parameters inside ``parameters.py``
- Input the following command in the terminal

    ``python fb.py``
