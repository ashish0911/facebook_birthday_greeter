import parameters
from time import sleep
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


options = Options()
options.add_argument("--disable-notifications")
driver = webdriver.Chrome(parameters.path_to_chromedriver, chrome_options=options)
driver.implicitly_wait(5)
driver.maximize_window()
driver.get(parameters.site)

username = driver.find_element_by_xpath('//*[@id="email"]')
username.send_keys(parameters.username)
sleep(2)

password = driver.find_element_by_xpath('//*[@id="pass"]')
password.send_keys(parameters.password)
sleep(3)

sign_in_button = driver.find_element_by_xpath('//*[@id="loginbutton"]')
sign_in_button.click()
sleep(1)

try:
    events_present = EC.presence_of_element_located((By.XPATH, '//*[@id="appsNav"]/ul/li/a/div[text()="Events"]')) #or //*[@title="Events"] can be used
    events = WebDriverWait(driver, 5).until(events_present)
    events.click()
except TimeoutException:
    print "Timed out waiting for page to load"

sleep(3)

try:
    birthday_present = EC.presence_of_element_located((By.XPATH, '//*[@data-key="birthdays"]'))  # //*[@id="entity_sidebar"]/div[3]/div[3]
    birthday = WebDriverWait(driver, 5).until(birthday_present)
    birthday.click()
except TimeoutException:
    print "Timed out waiting for page to load"

sleep(3)

urls = []
names = []

try:
    birthdaylist_present = EC.presence_of_element_located((By.XPATH, '//*[@id="birthdays_today_card"]'))
    WebDriverWait(driver, 5).until(birthdaylist_present)
    birthdaylist = driver.find_elements_by_xpath('//*[@id="birthdays_today_card"]/following-sibling::div/ul/li/div/div[2]/div/div[2]/div/div[2]/div/a')
    urls = [url.get_attribute("href") for url in birthdaylist]
    names = [name.text for name in birthdaylist]
except TimeoutException:
    print "Timed out waiting for page to load"

for url in urls:
    sleep(5)
    driver.get(url)
    try:
        messagebox = driver.find_element_by_xpath('//*[@id="pagelet_timeline_profile_actions"]/div[2]/a')
        messagebox.click()
        sleep(1)
        inputbox = driver.find_element_by_xpath('//*[@id="ChatTabsPagelet"]/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div[4]/div/div/div/span/div/div/div[2]/div')
        inputbox.send_keys(parameters.msg+Keys.ENTER)
        sleep(3)
        closebox = driver.find_element_by_xpath('//*[@id="ChatTabsPagelet"]/div/div[1]/div/div/div/div/div/div[1]/div/div[1]/div/ul/li[4]')
        closebox.click()
    except Exception:
        pass







